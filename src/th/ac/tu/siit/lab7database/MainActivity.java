package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends ListActivity {
	
	DBHelper dbHelper;
	SQLiteDatabase db;
	//cursor is a class that use for manage the retrieved records from database
	//It's similar to "pointer"
	Cursor cursor; 
	SimpleCursorAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = new DBHelper(this);
		db = dbHelper.getWritableDatabase();
		cursor = getAllContacts(); 
		//data is store in cursor 
		adapter = new SimpleCursorAdapter(this, R.layout.item, cursor, 
				new String[] {"ct_name", "ct_phone", "ct_type", "ct_email"},
				new int[] {R.id.tvName, R.id.tvPhone, R.id.ivPhoneType, R.id.tvEmail}, 0);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}
	
	private Cursor getAllContacts() {
		//db.query = execute a SELECT statement and return a cursor
		return db.query("contacts", //table name
				new String[] {"_id", "ct_name", "ct_phone", "ct_type", "ct_email"}, //list of column to be retrieve
				null, //condition for WHERE clause, "ct_name LIKE ?"
				null, //values for the condition, new String[] {"John%"}
				null, //GROUP BY
				null, //HAVING
				"ct_name asc"//ORDER BY
				
				); //SELECT _id,ct_name,ct_phone,ct_type,ct_email FROM contacts ORDER BY ct_name asc
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);
	}

	//when user presses the BACK button, we close the cursor and database
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cursor.close();
		db.close();
		dbHelper.close();
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, 
			int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			ContentValues v = new ContentValues(); //obj that storing data for insert
			v.put("ct_name", data.getStringExtra("name"));
			v.put("ct_phone", data.getStringExtra("phone"));
			v.put("ct_email", data.getStringExtra("email"));
			v.put("ct_type", data.getStringExtra("type"));
			db.insert("contacts", null, v);
			
			//refresh the ListView By 
			//(1) retrieving the record,
			//(2)set the new cursor to Adapter
			//(3)Call notifyDataSetChanged to update the ListView
			cursor = getAllContacts();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
		}
		
		else if(requestCode == 8888 && resultCode == RESULT_OK){
			
			ContentValues v = new ContentValues(); //obj that storing data for insert
			v.put("ct_name", data.getStringExtra("name"));
			v.put("ct_phone", data.getStringExtra("phone"));
			v.put("ct_email", data.getStringExtra("email"));
			v.put("ct_type", data.getStringExtra("type"));
			
			String selection = "_id = ?";
			int id = data.getIntExtra("id", 0); 
			String[] selectionArgs = { String.valueOf(id) }; 
			Log.v("DB", ""+id);
			db.update("contacts", v, selection, selectionArgs);
			cursor = getAllContacts();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
		}
		
			
		
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_new:
			Intent i = new Intent(this, AddNewActivity.class);
			startActivityForResult(i, 9999);
			return true;
		
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo a = (AdapterContextMenuInfo)item.getMenuInfo();
		long id = a.id; //primary key; column _id of the table
		int position = a.position;
		Intent i = new Intent(this,AddNewActivity.class);
		switch(item.getItemId()) {
		case R.id.action_edit:
			//Get the record at the position
			Cursor c = (Cursor)adapter.getItem(position);
			//Get the value of column ct_name
			String name = c.getString(c.getColumnIndex("ct_name"));
			String phone = c.getString(c.getColumnIndex("ct_phone"));
			String email = c.getString(c.getColumnIndex("ct_email"));
			int ctype = c.getInt(c.getColumnIndex("ct_type"));
			
			
			i.putExtra("name",name);
			i.putExtra("phone",phone);
			i.putExtra("email",email);
			i.putExtra("type",ctype); //int
			i.putExtra("id",id); //int
//			i.putExtra("id",position);
			startActivityForResult(i,8888);
			return true;
		case R.id.action_delete:
			String selection = "_id = ?";
			String[] selectionArgs = { String.valueOf(id) }; 
			db.delete("contacts", selection, selectionArgs);
			cursor = getAllContacts();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
			return true;
	
		}
		return super.onContextItemSelected(item);
	}
}
