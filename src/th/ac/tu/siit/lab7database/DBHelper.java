package th.ac.tu.siit.lab7database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//This is the class that manage the database tables
//One database is one file. It's a relational database
public class DBHelper extends SQLiteOpenHelper {
	
	private static final String DBNAME = "contacts.db";
	//When DBVersion is increase, the method update table will be called.
	private static final int DBVERSION = 1;
	
	public DBHelper(Context ctx) {
		//database name and database version
		super(ctx, DBNAME, null, DBVERSION);
	}

	//Call when the application is newly installed
	//No database file in the internal storage
	@Override
	public void onCreate(SQLiteDatabase db) {
		//the primary key of the table need to be _xxxx (e.g. "_id")
		//when we want to use this table with ListView
		String sql = "CREATE TABLE contacts (" +
				"_id integer primary key autoincrement, " +
				"ct_name text not null, " +
				"ct_phone text not null, " +
				"ct_type integer default 0, " +
				"ct_email text not null);";
		db.execSQL(sql);		
	}

	//Called when the database file is exists
	//but the DBVERSION was increased.
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//not practical one
		//we should use alter table.
		//this one is update by removing the current table and recreate a new one
		String sql = "DROP TABLE IF EXISTS contacts;";
		db.execSQL(sql);
		this.onCreate(db);
	}

}
